package io.pismo.api;

import java.io.IOException;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;

import io.pismo.base.entidade.Produto;
import io.vertx.core.Vertx;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import io.vertx.ext.unit.junit.VertxUnitRunner;

@RunWith(VertxUnitRunner.class)
public class VendaVerticletTest {

	private Vertx vertx;
	private Integer port;


	@BeforeClass
	public static void initialize() throws IOException {
		
	}

	@AfterClass
	public static void shutdown() {
		
	}

	/**
	 * Before executing our test, let's deploy our verticle.
	 * <p/>
	 * This method instantiates a new Vertx and deploy the verticle. Then, it waits in the verticle has successfully
	 * completed its start sequence (thanks to `context.asyncAssertSuccess`).
	 *
	 * @param context the test context.
	 */
	@Before
	public void setUp(TestContext context) throws IOException {
		vertx = Vertx.vertx();

		port = 8082;
		
		vertx.deployVerticle(VendaVerticle.class.getName());
	}

	/**
	 * This method, called after our test, just cleanup everything by closing the vert.x instance
	 *
	 * @param context the test context
	 */
	@After
	public void tearDown(TestContext context) {
		vertx.close(context.asyncAssertSuccess());
	}

	/**
	 * Let's ensure that our application behaves correctly.
	 *
	 * @param context the test context
	 */
	@Test
	public void testeMensagemInicial(TestContext context) {
		final Async async = context.async();

		vertx.createHttpClient().getNow(port, "localhost", "/", response -> {
			response.handler(body -> {
				context.assertTrue(body.toString().contains("compras-api"));
				async.complete();
			});
		});
	}

	@Test
	public void testeBuscarCompras(TestContext context) {
		Async async = context.async();
		vertx.createHttpClient().get(port, "localhost", "/api/compras")
		.putHeader("content-type", "application/json")
		.handler(response -> {
			context.assertEquals(response.statusCode(), 200);
			context.assertTrue(response.headers().get("content-type").contains("application/json"));
			response.bodyHandler(body -> {
				final Produto produto = Json.decodeValue(body.toString(), Produto.class);
				context.assertEquals(produto.getDescricao(), "Iphone 6S");
				context.assertEquals(produto.getCategoria(), "Teste");
				context.assertEquals(produto.getValor(), 5.0);
				context.assertNotNull(produto.getId());
				async.complete();
			});
		})
		.end();
	}
}
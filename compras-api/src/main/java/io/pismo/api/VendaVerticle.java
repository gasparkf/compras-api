package io.pismo.api;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import io.pismo.base.dao.DAO;
import io.pismo.base.entidade.Produto;
import io.pismo.base.entidade.Venda;
import io.pismo.util.RespostaUtil;
import io.pismo.util.Runner;
import io.pismo.util.SegurancaUtil;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.http.HttpServerResponse;
import io.vertx.core.json.Json;
import io.vertx.core.json.JsonArray;
import io.vertx.ext.sql.SQLConnection;
import io.vertx.ext.sql.UpdateResult;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;

public class VendaVerticle extends AbstractVerticle {
	
	
	public static void main(String[] args) {
		Runner.run(VendaVerticle.class);
	}

	@Override
	public void start() throws Exception {
		DAO.iniciar(vertx, "jdbc:postgresql://localhost:5432/pismoDB", "postgres", "org.postgresql.Driver", "postgres", 15);
		
		Router router = Router.router(vertx);

		router.get("/api/compras").handler(this::buscarTodos);
		router.route("/api/compras*").handler(BodyHandler.create());
		router.post("/api/compras").handler(this::gravar);
		router.post("/api/compras/vender").handler(this::vender);
		
		router.route("/").handler(routingContext -> {
			HttpServerResponse response = routingContext.response();
			response
			.putHeader("content-type", "text/html")
			.end("<h1>API para inserir e recuperar compras - compras-api</h1>");
		});

		vertx
		.createHttpServer()
		.requestHandler(router::accept)
		.listen(8082);
	}
	
	@Override
	public void stop() throws Exception {
		DAO.jdbcClient.close();
		super.stop();
	}
	
	private void buscarTodos(RoutingContext routingContext) {
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
				String sql= "SELECT * FROM venda";
				
				DAO.buscarTodos(sql, rs -> {
					if(rs.size() > 0){
						RespostaUtil.respostaErro(200, routingContext, rs.toString());
					}else{
						routingContext.response().setStatusCode(400).end();
					}
				});
			}
		});
	}
	
	private void gravar(RoutingContext routingContext) {
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
				final Venda compra = Json.decodeValue(routingContext.getBodyAsString(), Venda.class);
				
				Double total = compra.getProdutos().stream().mapToDouble(p -> p.getValor()).sum();
				compra.setTotal(total);
				
				DAO.jdbcClient.getConnection(res -> {
					if (res.succeeded()) {
		
						SQLConnection connection = res.result();
						
						StringBuilder sqlCompra = new StringBuilder("INSERT INTO venda (data, total) VALUES (");
						sqlCompra.append("'");
						sqlCompra.append(new SimpleDateFormat("yyyy-MM-dd").format(compra.getData()));
						sqlCompra.append("', ");
						sqlCompra.append(compra.getTotal());
						sqlCompra.append(")");
						
						connection.setAutoCommit(false, resh -> {
							if(!resh.succeeded()){
								RespostaUtil.respostaErro(400, routingContext);
							}
						});
						
						List<JsonArray> batch = new ArrayList<>();
						
						connection.update(sqlCompra.toString(), rhc ->{
							if(rhc.succeeded()){
								UpdateResult ur = rhc.result();
								Long pkCompra = ur.getKeys().getLong(0);
								for (Produto p : compra.getProdutos()) {
									JsonArray ja = new JsonArray();
									ja.add(pkCompra);
									ja.add(p.getDescricao());
									ja.add(p.getCategoria());
									ja.add(p.getValor());
									batch.add(ja);
								}
								
								connection.batchWithParams("INSERT INTO produto_venda (venda_id, descricao, categoria, valor) VALUES (?, ?, ?, ?)", batch, res2 -> {
									if (res2.succeeded()) {
										connection.commit(hc -> {
											if(hc.succeeded()){
												RespostaUtil.respostaErro(201, routingContext);
											}else{
												RespostaUtil.respostaErro(400, routingContext);
											}
										});
									} else {
										RespostaUtil.respostaErro(400, routingContext);
									}
									connection.close();
								});
									
							}else{
								RespostaUtil.respostaErro(400, routingContext);
							}
						});
					} else {
						RespostaUtil.respostaErro(400, routingContext);
					}
					
				});
			}
		});
	}
	
	private void vender(RoutingContext routingContext) {
		SegurancaUtil.autenticou(routingContext, ret -> {
			if(ret.booleanValue()){
				vertx.createHttpClient().get(8081, "localhost", "/api/produtos")
				.putHeader("content-type", "application/json")
				.putHeader("Authorization", "Basic YWRtaW46YWRtaW4=")
				.setTimeout(10000)
				.exceptionHandler(eh -> {
					RespostaUtil.respostaErro(503, routingContext);
				})
				.handler(response -> {
					if(response.statusCode() == 200){
						response.bodyHandler(body -> {
							
							String b = body.toString();
							JsonArray ja = new JsonArray(b);
							ArrayList<Produto> produtos = new ArrayList<>();
							for (Object item : ja) {
								Produto p = Json.decodeValue(item.toString(), Produto.class);
								produtos.add(p);
							}
							
							Venda venda = new Venda();
							venda.setData(new Date());
							venda.setProdutos(produtos);
							
							Double total = produtos.stream().mapToDouble(p -> p.getValor()).sum();
							venda.setTotal(total);
							
							DAO.jdbcClient.getConnection(res -> {
								if (res.succeeded()) {
		
									SQLConnection connection = res.result();
									
									String sqlCompra = "INSERT INTO venda (data, total) VALUES (?,?)";
									JsonArray param = new JsonArray();
									param.add(new SimpleDateFormat("yyyy-MM-dd").format(venda.getData()));
									param.add(venda.getTotal());
									
									connection.setAutoCommit(false, resh -> {
										if(!resh.succeeded()){
											RespostaUtil.respostaErro(400, routingContext);
										}
									});
									
									List<JsonArray> batch = new ArrayList<>();
									
									connection.updateWithParams(sqlCompra, param, rhc ->{
										if(rhc.succeeded()){
											UpdateResult ur = rhc.result();
											Long pkCompra = ur.getKeys().getLong(0);
											venda.setId(pkCompra);
											
											for (Produto p : venda.getProdutos()) {
												JsonArray ja2 = new JsonArray();
												ja2.add(pkCompra);
												ja2.add(p.getDescricao());
												ja2.add(p.getCategoria());
												ja2.add(p.getValor());
												batch.add(ja2);
											}
											
											connection.batchWithParams("INSERT INTO produto_venda (venda_id, descricao, categoria, valor) VALUES (?, ?, ?, ?)", batch, res2 -> {
												if (res2.succeeded()) {
													connection.commit(hc -> {
														if(hc.succeeded()){
															RespostaUtil.respostaErroEP(201, routingContext, venda);
														}else{
															RespostaUtil.respostaErro(400, routingContext);
														}
													});
												} else {
													RespostaUtil.respostaErro(400, routingContext);
												}
												connection.close();
											});
												
										}else{
											RespostaUtil.respostaErro(400, routingContext);
										}
									});
								} else {
									RespostaUtil.respostaErro(400, routingContext);
								}
								
							});
							
						});
					}else{
						RespostaUtil.respostaErro(400, routingContext);
					}
					
				})
				.end();
			}
		});
	}
	
}
